const jwt = require('jsonwebtoken')

const validateJWT = (req,res, next) => {

    //read token 
    const token = req.header('x-token');
  
    if(! token || token == 'null'){

        return res.status(401).json({
            ok: false,
            msg:' no token in request'
        })
    }

    try {

        const {uid} = jwt.verify(token, process.env.JWT_KEY)
        req.uid = uid;

        next();
    } catch (error) {
        console.log('errorrr: ',error)
        return res.status(401).json({
            ok:false,
            msg: 'invalid token'
        })
    }
} 

module.exports = {
    validateJWT
}