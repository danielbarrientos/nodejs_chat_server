/**
 path: api/login
  
 */
const { Router } = require('express');
const { check } = require('express-validator');
const { createUser, login, renewToken } = require('../controllers/auth');
const { inputValidate } = require('../middlewares/input-validate');
const { validateJWT } = require('../middlewares/validate-jwt');

const router = Router();

router.post('/new', [
    check('name','The name is required').not().isEmpty(),
    check('email','The email is required').normalizeEmail().isEmail().withMessage('it is not a valid email'),
    check('password','The password is required').isLength({ min: 5 }).withMessage('must be at least 5 chars long'),
    inputValidate
],createUser)

router.post('/', [
    check('email','The email is required').normalizeEmail().isEmail().withMessage('it is not a valid email'),
    check('password','The password is required').isLength({ min: 5 }).withMessage('must be at least 5 chars long'),
    inputValidate
],login)

router.get('/renew',validateJWT, renewToken);


module.exports = router;