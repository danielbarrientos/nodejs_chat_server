
const express = require('express');
const path = require('path');
require('dotenv').config();

//dbConfig
const { dbConnection } = require('./database/config');
dbConnection();

//App express
const app = express();


//lectura y parseo del body
app.use(express.json())

// Nodejs server
const server = require('http').createServer(app);
module.exports.io = require('socket.io')(server);
require('./sockets/socket');

//public path
const publicPath = path.resolve( __dirname, 'public');
app.use( express.static(publicPath))

//My routes
app.use('/api/login', require('./routes/auth'));
app.use('/api/users', require('./routes/users'));
app.use('/api/messages', require('./routes/messages'));

server.listen(process.env.PORT, (error)=>  {

    if(error) throw new Error(error);

    console.log('Server on port !!: ', process.env.PORT);
});
