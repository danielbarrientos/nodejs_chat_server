const { validationResult } = require("express-validator");


const inputValidate = (req,res, next ) => {

    const errors = validationResult(req)

    if( ! errors.isEmpty() ){
        return res.status(422).json({
            ok:false,
            errors: errors.mapped()
        })
    }

    next();
}

module.exports = {
    inputValidate
}