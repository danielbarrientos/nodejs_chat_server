
const { checkJWT } = require('../helpers/jwt');
const {io} = require('../index');
const {userConnected,userDisconnected,saveMessage} = require('../controllers/socket')
//Sockets messages
io.on('connection', client=> {

    console.log('connect');

    const [valid,uid] = checkJWT(client.handshake.headers['x-token'] )
    // verify authenticated   
    if(! valid) { return client.disconnect();}

    //client authenticated
     userConnected(uid);
    // enter the user to a room
    client.join(uid);
    // listen of client message
    client.on('private-message', async (payload) => {
        
         await saveMessage(payload)
        io.to(payload.for).emit('private-message', payload);
    })

    client.on('disconnect', () => { 
        console.log('disconnect')
        userDisconnected(uid)
    });

   
});