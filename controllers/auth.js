const { response } = require('express')
const bcrypt = require('bcryptjs');
const User = require('../models/user');
const { generateJWT } = require('../helpers/jwt');

const createUser = async (req,res = response) => {

    const {email,password } = req.body;

    try {
        const existsEmail = await User.findOne({email})
        if(existsEmail) {
            return res.status(409).json({
                ok: false,
                msg: 'The email is already in use'
            })
        }

        const user = new User(req.body)

        const salt = bcrypt.genSaltSync()
        user.password = bcrypt.hashSync(password,salt)

        await user.save()

        // Generate JWT
        const token = await generateJWT(user.id)
        res.json({
            ok: true,
            user,
            token
        })
        
    } catch (error) {
      
        res.status(500).json({
            ok:false,
            msg:'contact the administrator'
        })
    }
}


const login = async (req,res = response) => {

    const {email,password } = req.body;

    try {

        const userDB = await User.findOne({email})

        if( ! userDB){
            return res.status(404).json({
                ok: false,
                msg: 'Email not found '
            })
        }

        const validPassword = bcrypt.compareSync(password,userDB.password)

        if(! validPassword) {
            return res.status(400).json({
                ok: false,
                msg: 'invalid password'
            })
        }

        //generate JWT
        const token = await generateJWT(userDB.id);

        res.json({
            ok: true,
            user: userDB,
            token
        })
        
    } catch (error) {
        
        res.status(500).json({
            ok:false,
            msg:'contact the administrator'
        })
    }
}


const  renewToken = async(req, res = response) => {

    const uid = req.uid;

    const userDB = await User.findById(uid)

    if( ! userDB){
        return res.status(404).json({
            ok: false,
            msg: 'ivalid old token '
        })
    }

    const token = await generateJWT(userDB.id);

    res.json({
        ok: true,
        user: userDB,
        token
    })
}

module.exports = {
    createUser,
    login,
    renewToken
}

