const {response} = require("express");
const Message = require('../models/message');


const getMessages = async (req,res ) => {

    const myUid = req.uid;
    const messageOf = req.params.of;

    // const to = Number()

    const messages = await Message.find({ 
        $or: [{of: myUid,for: messageOf}, {of: messageOf,for: myUid}]
    })
    .sort({ createdAt:'desc'})
    .limit(30)
                       
    res.json({
        ok: true,
        messages
    })
}

module.exports = {
    getMessages
}