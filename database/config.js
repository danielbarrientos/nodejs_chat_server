const moongose = require('mongoose')

const dbConnection = async () => {
    try {
        await moongose.connect(process.env.DB_CNN, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex:true
        })    

        console.log('DB online');
    } catch (error) {
        console.log(error);
        throw new Error(' error in the data base - contact the administrator')
    }
}

module.exports = {
    dbConnection
}